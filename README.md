# Server Infrastruktur Docs

## Vorwort

Dieses Repository dient der AHF-internen Dokumentation für administrative Arbeiten an der eigenen Server-Infrastruktur. Darüber hinaus soll es aber auch eine Plattform sein, um Interessierten einen Einblick in die Erkenntnisse und Arbeitsweise des _AHFCampus_ zu ermöglichen.

Eigene Software-Projekte sind ausführlich in den entsprechenden Repositories dokumentiert und werden hier nur verlinkt.

Da der Kompetenzbereich der AHFCampus-Schulen offensichtlich in der Bildung und nicht in der IT-Branche liegt, ist das Ziel einen Großteil an IT-Lösungen sinnvoll auszulagern und als Dienstleistung zu beziehen. Im Einzelfall ist der Aufwand für Eigenbetrieb aber so gering, wodurch die im Folgenden beschriebene Infrastruktur entwickelt wurde.

## Überblick

Zentrale Plattform für das Betreiben unterschiedlicher selbst gehosteter Anwendungen ist [CapRover](https://caprover.com/). Ein einfach zu verwendender Deployment- und Web-Server-Manager für Docker-Container. Die Grafik zeigt den konzeptionellen Aufbau, wobei jeder Würfel für einen eigenen Docker-Container steht.

![Überblick Server Infrastruktur](./Assets/Infrastruktur.png)

### Selbstgehostete Anwendungen

- [konfiguriertes Strapi](https://gitlab.com/ahfcampus/cms): headless Content-Management-System für Webseiten
- [Webseiten](https://gitlab.com/ahfcampus/website-frontend-astro)
- [Plausible](https://plausible.io/): Traffic-Anlayse für Webseiten
- [PostgreSQL](https://www.postgresql.org/): Datenbank
- [Vaultwarden](https://github.com/dani-garcia/vaultwarden) (unoffizieller Bitwarden kompatibler Server): Passwort-Manager

### Backup-Konzept

_in Arbeit_

## Dokumentation

siehe [Wiki](https://gitlab.com/ahfcampus/server-infrastruktur-docs/-/wikis/home)